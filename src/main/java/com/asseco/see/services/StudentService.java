package com.asseco.see.services;

import com.asseco.see.model.Student;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/student")
public class StudentService {

    @Inject
    private ServiceBean serviceBean;

    @GET
    @Path("/list")
    public List<Student> getAllStudents() {
        return serviceBean.getStudents();
    }

    @POST
    @Path("/add/{name}/{grade}")
    public Response addStudent(
            @PathParam("name") String name,
            @PathParam("grade") Integer grade) {
        serviceBean.addStudent(name, grade);
        return Response.status(Response.Status.OK).build();
    }

    @DELETE
    @Path("/delete/{id}")
    public Response deleteStudent(@PathParam("id") String id) {
        serviceBean.deleteStudent(id);
        return Response.status(Response.Status.OK).build();
    }

    @PUT
    @Path("/edit/{id}/{name}/{grade}")
    public Response updateStudent(
            @PathParam("id") String id,
            @PathParam("name") String name,
            @PathParam("grade") Integer grade) {
        serviceBean.editStudent(id, name, grade);
        return Response.status(Response.Status.OK).build();
    }

}