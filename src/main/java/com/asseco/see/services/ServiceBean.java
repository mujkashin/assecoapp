package com.asseco.see.services;

import com.asseco.see.model.Student;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

@Singleton
public class ServiceBean {

    @PersistenceContext
    private EntityManager em;

    private MyHashtable studentTable;
    private boolean usingPersistentDb = false;

    @PostConstruct
    public void init() {
        studentTable = new MyHashtable();
    }

    public List<Student> getStudents(){
        if (usingPersistentDb) {
            TypedQuery<Student> q = em.createNamedQuery("student.findAll", Student.class);
            return q.getResultList();
        } else {
            return studentTable.getTableAsList();
        }
    }

    public void addStudent(String name, Integer grade){
        String uuid = UUID.randomUUID().toString();
        if (usingPersistentDb) {
            em.persist(new Student(uuid, name, grade));
        } else {
            try {
                studentTable.put(uuid, new Student(name, grade));
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void deleteStudent(String id){
        if (usingPersistentDb){
            TypedQuery<Student> q = em.createNamedQuery("student.findById", Student.class);
            q.setParameter("id", id);
            Student st = q.getSingleResult();
            em.remove(st);
        } else {
            studentTable.delete(id);
        }
    }

    public void editStudent(String id, String name, Integer grade){
        if (usingPersistentDb){
            TypedQuery<Student> q = em.createNamedQuery("student.findById", Student.class);
            q.setParameter("id", id);
            Student st = q.getSingleResult();
            st.setName(name);
            st.setGrade(grade);
            em.merge(st);
        } else {
            studentTable.edit(id, new Student(name, grade));
        }
    }
}

