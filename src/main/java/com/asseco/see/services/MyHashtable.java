package com.asseco.see.services;

import com.asseco.see.model.Student;
import java.util.ArrayList;
import java.util.List;

public class MyHashtable<K,V> {

    private TableEntry[] table;
    private int size = 0;
    private int initialCapacity = 16;

    MyHashtable() {
        this.table = new TableEntry[initialCapacity];
    }

    MyHashtable(int x) throws IllegalArgumentException{
        if (x < 1) {
            throw new IllegalArgumentException();
        }
        Integer capacity = nextPowerOf2(x);
        this.table = new TableEntry[capacity];
        initialCapacity = capacity;
    }

    public void put(K key, V value) throws IllegalArgumentException{
        if (key == null) {
            throw new IllegalArgumentException();
        }
        TableEntry<K,V> entry = new TableEntry<>(key, value, null);
        int index = getIndex(key);
        TableEntry existingEntry = table[index];
        if (existingEntry == null) {
            table[index] = entry;
            size++;
        } else {
            while (existingEntry.next != null) {
                if (existingEntry.key.equals(key)) {
                    existingEntry.value = value;
                    return;
                }
                existingEntry = existingEntry.next;
            }
            if (existingEntry.key.equals(key)) {
                existingEntry.value = value;
            } else {
                existingEntry.next = entry;
                size++;
            }
        }
    }

    public V get(K key) {
        TableEntry entry = table[key.hashCode() % size];
        while (entry != null) {
            if (entry.key.equals(key)) {
                return (V) entry.value;
            }
            entry = entry.next;
        }
        return null;
    }

    public void delete(K key) {
        int index = getIndex(key);
        TableEntry head = table[index];
        TableEntry prev = null;
        while (head.next != null) {
            if (head.key.equals(key)) {
                break;
            }
            prev = head;
            head = head.next;
        }
        if (head == null) {
            // not found
            return;
        }
        if (prev == null) {
            table[index] = head.next;
        } else {
            prev.next = head.next;
        }
        size --;
    }

    public void edit(K key, V value){
        int index = getIndex(key);
        TableEntry head = table[index];
        do {
            if (head.key.equals(key)) {
                head.value = value;
                break;
            }
            head = head.next;
        }
        while (head.next != null);
    }

    public List<Student> getTableAsList(){
        List<Student> list = new ArrayList<>();
        TableEntry head;
        for (int i = 0; i < initialCapacity; i++){
            head = table[i];
            if (head == null) {
                continue;
            }
            if (head.value instanceof Student) {
                Student st = (Student) head.value;
                st.setId((String) head.key);
                list.add(st);
            }
            while (head.next != null) {
                if (head.value instanceof Student) {
                    Student st = (Student) head.value;
                    st.setId((String) head.key);
                    list.add(st);
                }
                head = head.next;
            }
        }
        return list;
    }

    private int getIndex(K key) {
        return Math.abs(key.hashCode() % initialCapacity);
    }

    private int nextPowerOf2(int a) {
        int b = 1;
        while (b < a) {
            b = b << 1;
        }
        return b;
    }

    private static class TableEntry<K,V> {
        private final K key;
        private V value;
        private TableEntry<K,V> next;

        TableEntry(K key, V value, TableEntry<K,V> next) {
            this.key =  key;
            this.value = value;
            this.next = next;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public V setValue(V value) {
            if (value == null) {
                throw new NullPointerException();
            }

            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    }
}

