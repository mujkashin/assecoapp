package com.asseco.see.model;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "student", schema = "`asseco-see`")
@NamedNativeQueries({
        @NamedNativeQuery(name = "student.findAll", query = "SELECT * FROM student", resultClass = Student.class),
        @NamedNativeQuery(name = "student.findById", query = "SELECT * FROM student st WHERE st.id = :id", resultClass = Student.class)
})
@NamedQuery(name = "student.get", query = "SELECT t FROM Student t")
public class Student implements Serializable {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "grade")
    private Integer grade;

    public Student(){}

    public Student(String name, Integer grade) {
        this.name = name;
        this.grade = grade;
    }

    public Student(String id, String name, Integer grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }
}
