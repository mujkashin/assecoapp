var tableSize = 1;

function addStudent() {
    var name = document.getElementById("inputNameAdd").value;
    var grade = document.getElementById("inputGradeAdd").value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            populateTable();
        }
    };
    xhttp.open("POST", "http://localhost:9090/student/add/" + name + "/" + grade, true);
    xhttp.send();
}

function deleteStudent() {
    var id = document.getElementById("inputIdDelete").value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            populateTable();
        }
    };
    xhttp.open("DELETE", "http://localhost:9090/student/delete/" + id, true);
    xhttp.send();
}

function editStudent() {
    var id = document.getElementById("inputIdEdit").value;
    var name = document.getElementById("inputNameEdit").value;
    var grade = document.getElementById("inputGradeEdit").value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            populateTable();
        }
    };
    xhttp.open("PUT", "http://localhost:9090/student/edit/" + id + "/" + name + "/" + grade, true);
    xhttp.send();
}

function populateTable(){
    var xhttp = new XMLHttpRequest();
    var response;
    var table = document.getElementById("table");
    if (tableSize > 1) {
        emptyTable();
    }
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            response = JSON.parse(this.response);
            var row, cell1, cell2, cell3;
            for (var i = 0; i < response.length ; i++) {
                row = table.insertRow(tableSize);
                cell1 = row.insertCell(0);
                cell2 = row.insertCell(1);
                cell3 = row.insertCell(2);
                cell1.innerHTML = response[i].id;
                cell2.innerHTML = response[i].name;
                cell3.innerHTML = response[i].grade;
                tableSize++;
            }
        }
    };
    xhttp.open("GET", "http://localhost:9090/student/list", true);
    xhttp.send();
}

function emptyTable(){
    var table = document.getElementById("table");
    for (var i = 1; i < tableSize; i++){
        table.deleteRow(1);
    }
    tableSize = 1;
}